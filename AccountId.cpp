#include "AccountId.h"

string AccountId::LAST_ACCOUNT_ID_PATH = "accounts/last_ID";

AccountId::AccountId(){
    this->_date_created = getCurrentTime();
    trim(&this->_date_created);
    if (!fileExists(LAST_ACCOUNT_ID_PATH)) {
        this->_num = 0;
    }
    else {
        string last_id_file_text = readWholeFile(LAST_ACCOUNT_ID_PATH);
        this->_num = stoi(findParameterFromText(last_id_file_text, "LAST_ID"));
        this->_num++;
    }
    string updated_last_id_file = "LAST_ID=";
    updated_last_id_file.append(to_string(this->_num));
    if (!writeFile(updated_last_id_file, LAST_ACCOUNT_ID_PATH)) {
        throw "ERROR: It was not possible to write the last account ID file";
    }
}

AccountId::AccountId(int unique_id, string date_created){
    this->_num = unique_id;
    this->_date_created = date_created;
}

int AccountId::getUniqueNum(){
    return this->_num;
}

string AccountId::getDateTime(){
    return this->_date_created;
}
