#ifndef ACCOUNTID_H
#define ACCOUNTID_H
#include <string>
#include "utils.h"


using namespace std;

class AccountId {
    public:
        AccountId(); // Creates the account id;
        AccountId(int unique_id, string date_created); // Uses existing accound id;
        int getUniqueNum();
        string getDateTime();
    private:
        int _num;
        string _date_created;
        static string LAST_ACCOUNT_ID_PATH;
};

#endif
