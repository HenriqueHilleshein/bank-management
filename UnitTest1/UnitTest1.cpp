
#include "pch.h"
#include "CppUnitTest.h"
#include "..\Account.h"
#include "..\Account.cpp"
#include "..\AccountId.h"
#include "..\AccountId.cpp"
#include "..\utils.cpp"
#include "..\utils.h"
#include <iostream>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

string ACCOUNTS_FOLDER = "accounts/";

struct accountDetails {
	string string1;
	string string2;
	string datetime;
};

int checkCommonDetails(string account_details, accountDetails* correctAccountDetails) {
	string datetime = findParameterFromText(account_details, "Account created at", ": ");
	trim(&datetime);
	if (correctAccountDetails->datetime != datetime) {
		return -1;
	}
	if (stod(findParameterFromText(account_details, "Current Balance", ": ")) != 0) {
		return -1;
	}
	string account_id_str = findParameterFromText(account_details, "Account Number", ": ");
	int account_id;
	if (!str2int(&account_id, account_id_str)) {
		return -1;
	}
	return account_id; 
}

int checkCustomerDetails (Account* account, accountDetails* correctAccountDetails){
	string account_details = account->getAccountDetails();
	string string1 = findParameterFromText(account_details, "First name", ": ");
	trim(&string1);
	string string2 = findParameterFromText(account_details, "Last name", ": ");
	trim(&string2);
	if (correctAccountDetails->string1 != string1) {
		return -1;
	}
	if (correctAccountDetails->string2 != string2) {
		return -1;
	}
	return checkCommonDetails(account_details, correctAccountDetails);
}


int checkEnterpriseDetails(Account* account, accountDetails* correctAccountDetails) {
	string account_details = account->getAccountDetails();
	string string1 = findParameterFromText(account_details, "Business ID", ": ");
	trim(&string1);
	string string2 = findParameterFromText(account_details, "Company Name", ": ");
	trim(&string2);
	if (correctAccountDetails->string1 != string1) {
		return -1;
	}
	if (correctAccountDetails->string2 != string2) {
		return -1;
	}
	return checkCommonDetails(account_details, correctAccountDetails);
}

namespace UnitTest1
{
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(createAccountFolder) {
			// Create folder
			if (mkdir(Account::ACCOUNTS_PATH.c_str(), 0700) == 0) {
				Assert::IsTrue(true);
			} else {
				if (errno == EEXIST) {
					Assert::IsTrue(true);
				}
				else {
					Assert::Fail(L"It was not possible to create the directory accounts ID");
				}

			}
		}

		TEST_METHOD(createNewAccount) {
			// Create new account
			accountDetails cust1;
			accountDetails comp1;
			accountDetails cust2;
			accountDetails comp2;
			cust1.string1 = "Henrique";
			cust1.string2 = "Hilleshein";
			comp1.string1 = "1110235-2";
			comp1.string2 = "Symbio Finland Oy";
			cust2.string1 = "Jos�";
			cust2.string2 = "Silva";
			comp2.string1 = "2131234-3";
			comp2.string2 = "My Imaginary Company";

			cust1.datetime = getCurrentTime();
			trim(&cust1.datetime);
		    Customer customer1 = Customer(cust1.string1, cust1.string2);
			comp1.datetime = getCurrentTime();
			trim(&comp1.datetime);
			Enterprise enterprise1 = Enterprise(comp1.string1, comp1.string2);
			cust2.datetime = getCurrentTime();
			trim(&cust2.datetime);
			Account* customer2 = Account::createAccount("Customer", cust2.string1, cust2.string2);
			comp2.datetime = getCurrentTime();
			trim(&comp2.datetime);
			Account* enterprise2 = Account::createAccount("Enterprise", comp2.string1, comp2.string2);
			int accountid1 = checkCustomerDetails(&customer1, &cust1);
			int accountid2 = checkEnterpriseDetails(&enterprise1, &comp1);
			int accountid3 = checkCustomerDetails(customer2, &cust2);
			int accountid4 = checkEnterpriseDetails(enterprise2, &comp2);
			writeFile(to_string(accountid4), "where.txt");
			if (accountid1 == -1) {
				Assert::Fail(L"Account 1 was not created correctly");
			}
			if (accountid2 == -1) {
				Assert::Fail(L"Account 2 was not created correctly");
			}
			if (accountid3 == -1) {
				Assert::Fail(L"Account 3 was not created correctly");
			}
			if (accountid4 == -1) {
				Assert::Fail(L"Account 4 was not created correctly");
			}
			if ((accountid1 == accountid2) || (accountid1 == accountid3) || (accountid1 == accountid4) ||
				(accountid2 == accountid3) || (accountid2 == accountid4) ||
				(accountid3 == accountid4)) {
				Assert::Fail(L"Repeated account ID");
			}
			delete customer2;
			delete enterprise2;
			Assert::IsTrue(true);
		}

		TEST_METHOD(addMoney)
		{
			Customer customer = Customer("Henrique", "Hilleshein");
			double amount1 = 123.32;
			double amount2 = 1;
			double amount3 = 213213213.32312;
			double amount4 = 222222;
			double amount5 = 0; // Not accepted
			double amount6 = -1; // Not accepted
			double amount7 = -31232.4124; // Not accepted
			double amount8 = -213123213; // Not accepted
			customer.addMoney(amount1);
			customer.addMoney(amount2);
			customer.addMoney(amount3);
			customer.addMoney(amount4);
			try {
				customer.addMoney(amount5);
			}
			catch (...) {}
			try {
				customer.addMoney(amount6);
			}
			catch (...) {}
			try {
				customer.addMoney(amount7);
			}
			catch (...) {}
			try {
				customer.addMoney(amount8);
			}
			catch (...) {}
			string account_details = customer.getAccountDetails();
			double result = stod(findParameterFromText(account_details, "Current Balance", ": "));
			double correct_result = amount1 + amount2 + amount3 + amount4;
			
			Assert::AreEqual(correct_result, result, L"The final balance is wrong");
		}
		TEST_METHOD(WitdrawMoney)
		{
			Customer customer = Customer("Jose", "Bezerra");
			double addedMoney = 10000;
			customer.addMoney(addedMoney);
			//Now withdraw money
			double amount1 = 123.32;
			double amount2 = 1;
			double amount3 = 213213213.32312; // More than the account has
			double amount4 = 222222; // More than the account has
			double amount5 = 1534;
			double amount6 = 0; // Not accepted
			double amount7 = -1; // Not accepted
			double amount8 = -31232.4124; // Not accepted
			double amount9 = -213123213; // Not accepted
			customer.withdrawMoney(amount1);
			customer.withdrawMoney(amount2);
			customer.withdrawMoney(amount3);
			customer.withdrawMoney(amount4);
			customer.withdrawMoney(amount5);
			try {
				customer.addMoney(amount6);
			}
			catch (...) {}
			try {
				customer.addMoney(amount7);
			}
			catch (...) {}
			try {
				customer.addMoney(amount8);
			}
			catch (...) {}
			try {
				customer.addMoney(amount9);
			}
			catch (...) {}
			string accountDetails = customer.getAccountDetails();
			double result = stod(findParameterFromText(accountDetails, "Current Balance", ": "));
			double correct_result = addedMoney - (amount1 + amount2 + amount5);

			Assert::AreEqual(correct_result, result, L"The final balance is wrong");
		}
	};
}
