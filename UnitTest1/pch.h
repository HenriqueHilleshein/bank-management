// pch.h: This is a precompiled header file.
// Files listed below are compiled only once, improving build performance for future builds.
// This also affects IntelliSense performance, including code completion and many code browsing features.
// However, files listed here are ALL re-compiled if any one of them is updated between builds.
// Do not add files here that you will be updating frequently as this negates the performance advantage.

#ifndef PCH_H
#define PCH_H
#ifdef __linux__ 
#include <sys/stat.h>
#elif _WIN64
#include <direct.h>
#define  mkdir(x, y) _mkdir(x)
#else
#endif
// add headers that you want to pre-compile here

#endif //PCH_H
