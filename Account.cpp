#include "Account.h"

string Account::ACCOUNTS_PATH = "accounts/"; 

string Account::LAST_ACCOUNT_ID_PATH = Account::ACCOUNTS_PATH + "last_ID";

vector<string> Account::account_types = {"Customer", "Enterprise"};

Account::Account(){
    this->_balance = 0;
}

Account::Account(int unique_num, string date_created, double balance)
: account_id(unique_num, date_created){
    this->_balance = balance;
} 

Account* Account::getAccount(string account_id_num){
    Account* accountPtr = NULL;
    string filename = Account::ACCOUNTS_PATH;
    filename.append(account_id_num);
    if(!fileExists(filename)){
        return NULL;
    }
    string fileText = readWholeFile(filename);
    double balance = stod(findParameterFromText(fileText, "BALANCE"));   
    string string1 = findParameterFromText(fileText, "STRING1");
    string string2 = findParameterFromText(fileText, "STRING2");
    string date_created = findParameterFromText(fileText, "DATE_CREATED");
    string account_type = findParameterFromText(fileText, "ACCOUNT_TYPE");
    if (account_type == "Customer"){
        accountPtr = new Customer(string1, string2, stoi(account_id_num), balance, date_created);
    }
    if (account_type == "Enterprise"){
        accountPtr = new Enterprise(string1, string2, stoi(account_id_num), balance, date_created);  
    }
    return accountPtr;
}

Account* Account::createAccount(string account_type, string string1, string string2){
    Account* accountPtr = NULL;
    if (account_type == "Customer"){
        accountPtr = new Customer(string1, string2);  
    }
    if (account_type == "Enterprise"){
        accountPtr = new Enterprise(string1, string2);  
    }
    return accountPtr;
}

vector<string>* Account::getTypeParameters(string account_type){
    if (account_type == "Customer"){
        return &Customer::parameters;  
    }
    return &Enterprise::parameters;
}

void Account::addMoney(double amount){
    if (amount > 0) {
        this->_balance += amount;
        this->saveAccountDetails();
        return;
    }
    throw "Just positive numbers are accepted";
}
        
bool Account::withdrawMoney(double amount){
    if (amount <= 0) {
        throw "Just positive numbers are accepted";
    }
    double balance_after_withdraw = this->_balance - amount;
    if (balance_after_withdraw < 0){
        return false;
    }
    this->_balance = balance_after_withdraw;
    this->saveAccountDetails();
    return true;
}

void Account::createAccountId(){
}

string Account::commonAccountDetails(){
    string details = "Account Number: ";
    details.append(to_string(this->account_id.getUniqueNum()));
    details.append("\nAccount created at: ");
    details.append(this->account_id.getDateTime());
    details.append("\nCurrent Balance: ");
    details.append(to_string(this->_balance));
    return details;
}

void Account::writeAccountFile(string string1, string string2, string account_type){
    string fileAccountText = "STRING1=";
    fileAccountText.append(string1);
    fileAccountText.append("\nSTRING2=");
    fileAccountText.append(string2);
    fileAccountText.append("\nDATE_CREATED=");
    fileAccountText.append(this->account_id.getDateTime());
    fileAccountText.append("\nBALANCE=");
    fileAccountText.append(to_string(this->_balance));
    fileAccountText.append("\nACCOUNT_TYPE=");
    fileAccountText.append(account_type);
    string filename = Account::ACCOUNTS_PATH;
    filename.append(to_string(this->account_id.getUniqueNum()));
    if(!writeFile(fileAccountText, filename)){
        throw "ERROR: It was not possible to save or update the account information to the database";
    }
}

vector<string> Enterprise::parameters = {"Business ID", "Company Name"};

Enterprise::Enterprise(string business_id, string company_name){
    this->_business_id = business_id;
    this->_company_name = company_name;
    this->createAccountId();
    this->saveAccountDetails();
}

Enterprise::Enterprise(string business_id, string company_name, int account_id_num, double balance, string date_created)
: Account (account_id_num, date_created, balance){
    this->_business_id = business_id;
    this->_company_name = company_name;
}

string Enterprise::getAccountDetails(){
    string details = this->parameters[0];
    details.append(": ");
    details.append(this->_business_id);
    details.append("\n");
    details.append(this->parameters[1]);
    details.append(": ");
    details.append(this->_company_name);
    details.append("\n");
    details.append(this->commonAccountDetails());
    return details;
}

void Enterprise::saveAccountDetails(){
    this->writeAccountFile(this->_business_id, this->_company_name, "Enterprise");
}

vector<string> Customer::parameters = {"First name", "Last name"};

Customer::Customer(string first_name, string last_name){
    this->_first_name = first_name;
    this->_last_name = last_name;
    this->createAccountId();
    this->saveAccountDetails();
}

Customer::Customer(string first_name, string last_name, int account_id_num, double balance, string date_created)
: Account (account_id_num, date_created, balance){
    this->_first_name = first_name;
    this->_last_name = last_name;
}

string Customer::getAccountDetails(){
    string details = this->parameters[0];
    details.append(": ");
    details.append(this->_first_name);
    details.append("\n");
    details.append(this->parameters[1]);
    details.append(": ");
    details.append(this->_last_name);
    details.append("\n");
    details.append(this->commonAccountDetails());
    return details;
}

void Customer::saveAccountDetails(){
    this->writeAccountFile(this->_first_name, this->_last_name, "Customer");
}
