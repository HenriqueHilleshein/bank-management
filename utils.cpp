#ifdef __linux__ 
#define ctime_s(x, y, z) ctime_r(z,x)
#else
#endif

#include "utils.h"

using namespace chrono;

bool str2int(int * target, string num_str){
    try {
        *target = stoi(num_str);
	return true;
    }
    catch(...){};
    return false;
}

bool str2uint(unsigned int* target, string num_str){
    try {
        *target = stoul(num_str);
        return true;
    }
    catch (...) {};
    return false;
}


bool str2double(double * target, string num_str){
    try {
        *target = stod(num_str);
	    return true;
    }
    catch(...){}
    return false;
}

string &ltrim(string &s)
{
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
        return !std::isspace(ch);
        }));
    return s;
}

string &rtrim(string &s)
{
    s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
        return !std::isspace(ch);
        }).base(), s.end());
    return s;
}

void trim(string *str) {
    ltrim(rtrim(*str));
}

string findParameterFromText(string text, string parameter, string token) {
    size_t posParameter = text.find(parameter);
    if (posParameter != string::npos){
        string aux = text.substr(posParameter + parameter.size());
        size_t posEq = aux.find(token);
        if (posEq != string::npos){
            size_t posEnd = aux.find("\n");
            string value = aux.substr(posEq + 1, posEnd - posEq -1);
            trim(&value);
            return value;
        }
         
    }
    return "Not Found";
}

bool fileExists (const string& name) {
    struct stat buffer;   
    return (stat (name.c_str(), &buffer) == 0); 
}

string readWholeFile(string filename) {
    ifstream t(filename);
    string str;
    t.seekg(0, ios::end);   
    std::ifstream::streampos filesize = t.tellg();
    str.reserve(filesize);
    t.seekg(0, ios::beg);
    str.assign((istreambuf_iterator<char>(t)),
                istreambuf_iterator<char>());
    return str;
}

bool writeFile(string text, string filename){
    ofstream fw(filename, std::ofstream::out);

    if (!fw.is_open()){
        return false;
    }
    fw << text;
    fw.close();
    return true;
}

string getCurrentTime()
{
    system_clock::time_point today = system_clock::now();
    time_t tt = system_clock::to_time_t ( today );
    char datetime[26] = {};
    ctime_s(datetime, 26, &tt);
    return datetime;
}
