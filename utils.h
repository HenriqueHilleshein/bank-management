#ifndef UTILS_H
#define UTILS_H
#include <chrono>
#include <ctime> 
#include <string>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <sys/stat.h>
#include <stdexcept>

using namespace std;

bool str2int(int * target, string num_str);

bool str2uint(unsigned int* target, string num_str);

bool str2double(double * target, string num_str);

string &ltrim(string &s);

string &rtrim(string &s);

void trim(string *str);

string findParameterFromText(string text, string parameter, string token = "=");

bool fileExists (const string& name);

string readWholeFile(string filename);

bool writeFile(string text, string filename);

string getCurrentTime();


#endif
