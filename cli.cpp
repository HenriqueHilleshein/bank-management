#ifdef __linux__ 
#include <sys/stat.h>
#elif _WIN64
#include <direct.h>
#define  mkdir(x, y) _mkdir(x)
#else
#endif
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include "Account.h"
#include "clearscreen.h"

void skipTwoLines(){
    cout << "\n" << endl;
}


void callSupportMessage() {
    cout << "PROGRAM ENDED: Call the support." << endl;
}

void unexpectedError() {
    cout << "An unexpected error ocurred!" << endl;
    callSupportMessage();
}

void notIntegerMessage(){
    cout << "Invalid action. Just integers numbers are accepted" << endl;
    skipTwoLines();
}

void notFloatMessage(){
    cout << "Invalid action. Just float numbers are accepted" << endl;
    skipTwoLines();
}

void cliAddMoney(Account * account_ptr){
    clearScreen();
    std::string amount_str;
    cout << "What much money would like to add to the account?" << endl;
    getline(cin, amount_str);
    trim(&amount_str);
    double amount;
    if (!str2double(&amount, amount_str)){
       notFloatMessage();   
       return;
    }
    try {
        account_ptr->addMoney(amount);
        clearScreen();
        cout << "DONE!!" << endl;
    }
    catch (char* c) {
        cout << c << endl;
    }
    skipTwoLines();
}

void cliWithdrawMoney(Account * account_ptr){
    clearScreen();
    string amount_str;
    cout << "What much money would like to withdraw from the account?" << endl;
    getline(cin, amount_str);
    trim(&amount_str);
    double amount;
    if (!str2double(&amount, amount_str)){
       notFloatMessage();   
       return;
    }
    clearScreen();
    try {
        if (account_ptr->withdrawMoney(amount)) {
            cout << "DONE!!" << endl;
        }
        else {
            cout << "Not enough funds for this operation" << endl;
        }
    }
    catch (char* c) {
        cout << c << endl;
    }

    skipTwoLines();
}

void cliAccessAccount(){
    string account_id;
    clearScreen();
    cout << "What is the account ID?" << endl;
    getline(cin, account_id);
    trim(&account_id);
    Account* account_ptr = Account::getAccount(account_id);
    if (!account_ptr){
        cout << "Account does not exist" << endl;
        skipTwoLines();
        return;
    }
    clearScreen();
    cout << "Account details:" << endl;
    cout << account_ptr->getAccountDetails() << endl;
    skipTwoLines();
    string action;
    cout << "What would you like to do with account? ID = " << account_id << endl;
    cout << "0 - Add money" << endl;
    cout << "1 - Withdraw money" << endl;
    cout << "2 - Go back" << endl;
    getline(cin, action);
    trim(&action);
    unsigned int selected_option;
    if (!str2uint(&selected_option,action)){
       notIntegerMessage();
       delete account_ptr;   
       return;
    }
    switch (selected_option){
        case 0:
        cliAddMoney(account_ptr);
        break;
	case 1:
        cliWithdrawMoney(account_ptr);
         break;
    case 2:
        clearScreen();
        break;
    default:
        cout << "Chosen action does not exist" << endl;
        skipTwoLines();
    };
    delete account_ptr;
    return;
}

void cliCreateAccount(){
    clearScreen();
    cout << "What is the account type?" << endl;
    vector<string>::iterator it =  Account::account_types.begin();
    int index = 0;
    for (; it != Account::account_types.end(); ++it){
        cout << to_string(index) << " - " << *it << endl;
       	index++;
    }
    string answer;
    getline(cin, answer);

    trim(&answer);
    unsigned int selected_option;
    if (!str2uint(&selected_option, answer)){
        notIntegerMessage();   
        return;
    }
    if (selected_option > (Account::account_types.size()-1)){
        cout << "Selected account type does not exist" << endl;
        skipTwoLines();
        return;
    }
    string chosen_account_type = Account::account_types[selected_option]; 
    string string1; 
    string string2; 
    vector<string>* parameters = Account::getTypeParameters(chosen_account_type);
    clearScreen();
    cout << "What is the " << (*parameters)[0] << "?" << endl;
    getline(cin, string1);
    trim(&string1);
    clearScreen();
    cout << "What is the " << (*parameters)[1] << "?" << endl;
    getline(cin, string2);
    trim(&string2);
    Account* new_account = Account::createAccount(chosen_account_type, string1, string2);
    clearScreen();
    cout << "The new account details:" << endl;
    cout << new_account->getAccountDetails() << endl;
    delete new_account;
    skipTwoLines();
    return;
}


int main(){
    if (!(mkdir(Account::ACCOUNTS_PATH.c_str(), 0700) == 0)) {
        if (errno != EEXIST) {
            cout << "It was not possible to access of create the directory: ";
            cout << Account::ACCOUNTS_PATH << endl;
            return -1; 
        }

    }
    cout << "Welcome!" << endl;
    while(1){
        cout << "What would you like to do?" << endl;
        cout << "0 - Create a new account" << endl;
        cout << "1 - Access an account" << endl;
        cout << "2 - Exit" << endl;
        string answer;
        getline(cin, answer);
        trim(&answer);
        unsigned int selected_option;
        if (!str2uint(&selected_option,answer)){
            notIntegerMessage();
            continue;
        }
        switch (selected_option){
            case 0:
                try {
                    cliCreateAccount();
                }
                catch (char * err) {
                    cout << err << endl;
                    callSupportMessage();
                    return -1;
                }
                catch (...) {
                    unexpectedError();
                    return -1;
                }

                break;
            case 1:
                try {
                    cliAccessAccount();
                }
                catch (char* err) {
                    cout << err << endl;
                    callSupportMessage();
                    return -1;
                }
                catch (...) {
                    unexpectedError();
                    return -1;
                }
                break;
            case 2:
                return 0;
            default:
                cout << "Selected option does not exist. Please use one of the available options" << endl;
                skipTwoLines();
        }
        
    }
    return 0;
}
