cmake_minimum_required(VERSION 3.4.0 FATAL_ERROR)

set(PROJECT_NAME bank-management)

project(${PROJECT_NAME})

set(CMAKE_CXX_STANDARD 11)
################################################################################
# Source groups
################################################################################
set(Header_Files
    "Account.h"
    "AccountId.h"
    "utils.h"
    "clearscreen.h"
)
source_group("Header Files" FILES ${Header_Files})

set(Source_Files
    "Account.cpp"
    "AccountId.cpp"
    "cli.cpp"
    "utils.cpp"
    "clearscreen.cpp"
)
source_group("Source Files" FILES ${Source_Files})

set(ALL_FILES
    ${Header_Files}
    ${Source_Files}
)

################################################################################
# Target
################################################################################
add_executable(${PROJECT_NAME} ${ALL_FILES})

if(UNIX AND NOT APPLE)
    find_package(Curses REQUIRED)
    include_directories( ${CURSES_INCLUDE_DIR} )
    set(CURSES_NEED_NCURSES TRUE)
    target_link_libraries(${PROJECT_NAME} ncurses)
endif()

