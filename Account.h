#ifndef ACCOUNT_H
#define ACCOUNT_H
#include <string>
#include "utils.h"
#include <fstream>
#include <vector>
#include "AccountId.h"

using namespace std;

class Account
{
    public:
	Account();
	Account(int unique_num, string date_created, double balance); 
        static Account* getAccount(string account_id_num);
        static Account* createAccount(string account_type, string string1, string string2);
        static vector<string>* getTypeParameters(string account_type);
        virtual string getAccountDetails() = 0;
        virtual void saveAccountDetails() = 0;
        void addMoney(double amount);
        bool withdrawMoney(double amount);
        virtual ~Account(){};
        static vector<string> account_types;
        static string ACCOUNTS_PATH;
    protected:
        void createAccountId();
        string commonAccountDetails();
        void writeAccountFile(string string1, string string2, string account_type);

    private:
        static string LAST_ACCOUNT_ID_PATH;
        AccountId  account_id;
        double _balance; // Double as someone could be very rich. More than all the money in the world
};
class Enterprise: public Account
{
    public:
        Enterprise(string business_id, string company_name);
        Enterprise(string business_id, string company_name, int account_id_num, double balance, string date_created);
        string getAccountDetails();
        void saveAccountDetails();
        ~Enterprise(){};
        static vector<string> parameters;
    private:
        string _business_id;
        string _company_name;
};
class Customer: public Account
{
    public:
        Customer(string first_name, string last_name);
        Customer(string first_name, string last_name, int account_id_num, double balance, string date_created);
        string getAccountDetails();
        void saveAccountDetails();
        ~Customer(){};
        static vector<string> parameters;
    private:
        string _first_name;
        string _last_name;
};
#endif
